import React, { FC, useRef, useState, useEffect } from 'react';

import { Paper } from '@material-ui/core';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import TextField from '@material-ui/core/TextField';

import { connect, ConnectedProps } from 'react-redux';

import './App.css';
import { CheckIp, CheckUsersIp } from './store/ips/actions';
import { IpAddress } from './store/ips/types';
import { ApplicationState } from './store';
import { isIpValid, isURLValid } from './helpers';
import IpInfoBlock from './components/IpInfoBlock'

interface AppProps extends PropsFromRedux {

}

const renderIpList = (ipAddresses: IpAddress[], ipPickedFromHistory: IpAddress | null, setIpPickedFromHistory: React.Dispatch<React.SetStateAction<IpAddress | null>>): JSX.Element => {
    return <Paper className="results-list"><List>
        {ipAddresses.map((ipAddress, index) => (
            <ListItem
                selected={ipAddress.ip === ipPickedFromHistory?.ip}
                button={true}
                key={`ipAddress-${index}`}
                onClick={() => setIpPickedFromHistory(ipAddress)}
            >
                {ipAddress.ip}
            </ListItem>
        ))}
    </List></Paper>
}

const App: FC<AppProps> = (props: AppProps): JSX.Element => {
    const { ips, CheckIp, CheckUsersIp } = props;
    const { history, userIp, isUserIpLoading, isEnteredIpLoading } = ips;
    const [isIpCorrect, setIsIpCorrect] = useState(true);
    const [ipPickedFromHistory, setIpPickedFromHistory] = useState<IpAddress | null>(null);

    const ipInputRef = useRef<HTMLInputElement>(null);

    React.useEffect(() => {
        CheckUsersIp();
    }, []);

    const handleSearchStart = (): void => {
        if (isIpValid(ipInputRef?.current?.value as string) || isURLValid(ipInputRef?.current?.value as string)) {
            CheckIp(ipInputRef?.current?.value);
            setIpPickedFromHistory(null);
        } else {
            setIsIpCorrect(false)
        }
    }

    return (
        <div className="screen">
            <aside className="sidebar">
                {renderIpList(ips.history, ipPickedFromHistory, setIpPickedFromHistory)}
            </aside>
            <main className="content">
                <IpInfoBlock
                    title="Your IP Info"
                    isLoading={isUserIpLoading}
                    ipAddress={userIp}
                />
                <div className="ip-search">
                    <TextField
                        className="ip-search__input"
                        inputRef={ipInputRef}
                        variant="outlined"
                        onKeyPress={(event) => {
                            if (!isIpCorrect) { setIsIpCorrect(true); }
                            if (event.key === "Enter" && ipInputRef !== null && ipInputRef?.current?.value) {
                                handleSearchStart();
                            }
                        }}
                        error={!isIpCorrect}
                        helperText={!isIpCorrect ? "Ip or website address is incorrect" : ""}
                    />
                    <button
                        onClick={() => {
                            if (ipInputRef !== null && ipInputRef?.current?.value) {
                                handleSearchStart();
                            }
                        }}
                    >
                        Search
                    </button>
                </div>
                <IpInfoBlock
                    title="Last searched/picked IP Info"
                    isLoading={isEnteredIpLoading}
                    ipAddress={ipPickedFromHistory || history[history.length - 1]}
                />
            </main>
        </div>
    );
}

const mapStateToProps = (state: ApplicationState) => ({
    ips: state.ips,
});
const actions = { CheckIp, CheckUsersIp }
const connector = connect(mapStateToProps, actions);
type PropsFromRedux = ConnectedProps<typeof connector>

export default connector(App);
