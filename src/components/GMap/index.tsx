import React, { FC } from 'react';

import { CircularProgress } from '@material-ui/core';

import { GoogleMap, withScriptjs, withGoogleMap, Marker } from 'react-google-maps';

import GMapProps from './types';
import { IpAddress } from '../../store/ips/types';



const Map: FC<GMapProps> = (props: GMapProps): JSX.Element => {
    const { latitude, longitude } = props.ipAddress as IpAddress;

    return (
        <GoogleMap
            defaultZoom={10}
            defaultCenter={{lat: 45.421532, lng: -75.697189}}
            center={{lat: latitude, lng: longitude}}
        >
            <Marker position={{lat: latitude, lng: longitude}} />
        </GoogleMap>
    )
}

const WrappedMap: any = withScriptjs(withGoogleMap(Map));

const GMap: FC<GMapProps> = (props: GMapProps): JSX.Element => {
    const { ipAddress, isLoading } = props;

    return (
        <>
        {isLoading ? <CircularProgress /> : ipAddress ? !ipAddress?.latitude || !ipAddress?.longitude ? "Latitude or Longitude improper. Probably there's no such IP registered" : (
            <WrappedMap
                ipAddress={props.ipAddress as IpAddress}
                googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=${process.env.REACT_APP_GOOGLE_API_KEY}`}
                loadingElement={<div style={{height: '100%'}} />}
                containerElement={<div style={{height: '100%', width: '100%'}} />}
                mapElement={<div style={{height: '100%'}} />}
            />
        ) : "No data to display"}
        </>
    )
}

export default GMap;