import { IpAddress } from "../../store/ips/types";

export default interface GMapProps {
    ipAddress?: IpAddress;
    isLoading?: boolean;
}