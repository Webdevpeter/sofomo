import { IpAddress } from "../../store/ips/types";

export default interface IpInfoProps {
    ipAddress?: IpAddress;
    isLoading?: boolean;
}