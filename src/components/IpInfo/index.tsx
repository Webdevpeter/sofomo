import React, { FC } from 'react';

import { Paper } from '@material-ui/core';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import { CircularProgress } from '@material-ui/core';

import './styles.css';
import IpInfoProps from './types';

const IpInfo: FC<IpInfoProps> = (props: IpInfoProps): JSX.Element => {
    const { ipAddress, isLoading } = props;

    return (
        <Paper className="ip-info__details">
            {isLoading ? <CircularProgress /> : ipAddress ? (
                <List>
                    <ListItem>
                        Ip: {ipAddress.ip}
                    </ListItem>
                    <ListItem>
                        Type: {ipAddress.type}
                    </ListItem>
                    <ListItem>
                        Continent: {ipAddress.continent_name}
                    </ListItem>
                    <ListItem>
                        Country: {ipAddress.country_name}
                    </ListItem>
                    <ListItem>
                        City: {ipAddress.city}
                    </ListItem>
                    <ListItem>
                        Latitude: {ipAddress.latitude}
                    </ListItem>
                    <ListItem>
                        Longitude: {ipAddress.longitude}
                    </ListItem>
                </List>
            ) : "No data to display"}
        </Paper>
    )
}

export default IpInfo;