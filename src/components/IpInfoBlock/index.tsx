import React, { FC } from 'react';

import GMap from '../GMap';
import IpInfo from '../IpInfo';
import IpInfoBlockProps from './types';

import './styles.css'

const IpInfoBlock: FC<IpInfoBlockProps> = (props: IpInfoBlockProps): JSX.Element => {
    const { title, ...rest } = props;
    return (
        <>
            {title ? (<h2 className="ip-info__title">{title}</h2>) : ""}
            <div className="ip-info">
                <div className="ip-info__map">
                    <GMap {...rest} />
                </div>
                <IpInfo {...rest} />
            </div>
        </>
    )
};

export default IpInfoBlock;