import { IpAddress } from "../../store/ips/types";

export default interface IpInfoBlockProps {
    ipAddress?: IpAddress;
    isLoading?: boolean;
    title?: string;
}