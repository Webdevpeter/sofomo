import { createStore, applyMiddleware, Store } from 'redux';
// import { routerMiddleware } from 'connected-react-router';
import { composeWithDevTools } from 'redux-devtools-extension';
// import { History } from 'history';
import createSagaMiddleware from 'redux-saga';

import { ApplicationState, reducers } from './store';
import rootSaga from './sagas/index';

const sagaMiddleware = createSagaMiddleware();

// export let history: History;
// export let store: Store<ApplicationState>;

export function configureStore(/*history: History, */initialState?: ApplicationState): Store<ApplicationState> {

    const composeEnhancers = composeWithDevTools({});
    let middleware = [sagaMiddleware/*, routerMiddleware(history)*/];

    const store = createStore(
        reducers(/*history*/),
        initialState,
        composeEnhancers(applyMiddleware(...middleware))
    );

    sagaMiddleware.run(rootSaga);

    return store;
}

let store = configureStore();
export default store;

