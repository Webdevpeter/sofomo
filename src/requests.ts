import axios from 'axios';


const apiUrl = 'http://api.ipstack.com/';


const GetIpInfo = (ip: string) => {
    return axios.get(`${apiUrl}${ip}?access_key=${process.env.REACT_APP_IPSTACK_APIKEY}`);
}

const GetUserIpInfo = () => {
    return axios.get(`${apiUrl}check?access_key=${process.env.REACT_APP_IPSTACK_APIKEY}`);
}

export { GetIpInfo, GetUserIpInfo };