const isIpValid = (ip: string): boolean => {
    const splitOctets = ip.split('.');
    if (splitOctets.length < 4) {
        return false
    }

    let answer = true;

    for (let i = 0; i < splitOctets.length; i++) {
        const octet = splitOctets[i];
        const isNum = /^\d+$/.test(octet);

        if (!isNum || Number(octet) < 0 || Number(octet) > 255) {
            answer = false;
            break;
        }
    }

    return answer;
}

const isURLValid = (str: string): boolean => {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
  }

export { isIpValid, isURLValid };