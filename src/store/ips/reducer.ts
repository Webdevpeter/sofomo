import { Reducer } from 'redux';

import IpsState from './types';

const initState = {
    history: [],
    isUserIpLoading: false,
    isEnteredIpLoading: false,
};

const reducer: Reducer<IpsState> = (state: IpsState = initState, action): IpsState => {
    const { payload } = action;

    switch (action.type) {
        case '@@IPS/CHECK_IP':
            return { ...state, isEnteredIpLoading: true}
        case '@@IPS/ADD_TO_HISTORY':
            return { ...state, isEnteredIpLoading: false, history: [...state.history, payload.ipAddress] };
        case '@IPS/CHECK_USER_IP':
            return { ...state, isUserIpLoading: true}
        case '@IPS/CHECK_USER_IP_SUCCESS':
            return { ...state, isUserIpLoading: false, ...payload}
        default:
            return { ...state };
    }
};

export { reducer as ips };

export default reducer;
