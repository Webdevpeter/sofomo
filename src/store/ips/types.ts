import { Action } from 'redux';

export interface IpAddress {
    ip: string;
    city: string;
    continent_name: string;
    country_name: string;
    latitude: number
    longitude: number
    type: string;
}

export default interface IpsState {
    history: IpAddress[];
    userIp?: IpAddress;
    isUserIpLoading: boolean;
    isEnteredIpLoading: boolean;
}

export interface CheckIpAction extends Action {
    type: '@@IPS/CHECK_IP',
    payload: {
        ipAddress: string;
    }
}

export interface AddIpToHistoryAction extends Action {
    type: '@@IPS/ADD_TO_HISTORY',
    payload: {
        ipAddress: IpAddress;
    }
}

export interface CheckUsersIpAction extends Action {
    type: '@IPS/CHECK_USER_IP';
}

export interface CheckUsersIpSuccessAction extends Action {
    type: '@IPS/CHECK_USER_IP_SUCCESS';
    payload: {
        userIp: IpAddress;
    }
}