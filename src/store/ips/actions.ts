import { ActionCreator } from 'redux';

import {
    CheckIpAction, AddIpToHistoryAction, IpAddress, CheckUsersIpAction, CheckUsersIpSuccessAction
} from './types';

export const CheckIp: ActionCreator<CheckIpAction> = (ipAddress: string): CheckIpAction => ({
    type: '@@IPS/CHECK_IP',
    payload: {
        ipAddress
    }
});

export const AddIpToHistory: ActionCreator<AddIpToHistoryAction> = (ipAddress: IpAddress): AddIpToHistoryAction => ({
    type: '@@IPS/ADD_TO_HISTORY',
    payload: {
        ipAddress
    }
});

export const CheckUsersIp: ActionCreator<CheckUsersIpAction> = (): CheckUsersIpAction => ({
    type: '@IPS/CHECK_USER_IP',
});


export const CheckUsersIpSuccess: ActionCreator<CheckUsersIpSuccessAction> = (userIp: IpAddress): CheckUsersIpSuccessAction => ({
    type: '@IPS/CHECK_USER_IP_SUCCESS',
    payload: {
        userIp
    }
})