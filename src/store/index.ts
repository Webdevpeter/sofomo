import { combineReducers, Reducer } from 'redux';

// import { connectRouter, RouterState } from 'connected-react-router';

import IpsState from './ips/types';
import { ips } from './ips/reducer';

export interface ApplicationState {
    ips: IpsState;
    // router?: RouterState;
}

export const reducers = (/*history*/): Reducer<ApplicationState> =>
    combineReducers<ApplicationState>({
        // router: connectRouter(history),
        ips,
    });
