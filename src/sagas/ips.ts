import { takeEvery, call, fork, put } from 'redux-saga/effects';
import { GetIpInfo, GetUserIpInfo } from '../requests';
import { AddIpToHistory, CheckUsersIpSuccess } from '../store/ips/actions';
import { CheckIpAction } from '../store/ips/types';

function* checkIpDispatch(action: CheckIpAction): any {
    try {
        const { ipAddress } = action.payload;
        
        const response = yield call(GetIpInfo, ipAddress);
        const {
            ip,
            city,
            continent_name,
            country_name,
            latitude,
            longitude,
            type,
            ...rest
        } = response.data

        yield put(AddIpToHistory({
            ip,
            city,
            continent_name,
            country_name,
            latitude,
            longitude,
            type
        }));
    } catch (e) {

    }
}

function* watchCheckIpDispatch(): any {
    yield takeEvery('@@IPS/CHECK_IP', checkIpDispatch);
}


function* checkUserIpDispatch(): any {
    try {
        
        const response = yield call(GetUserIpInfo);
        const {
            ip,
            city,
            continent_name,
            country_name,
            latitude,
            longitude,
            type,
            ...rest
        } = response.data

        yield put(CheckUsersIpSuccess({
            ip,
            city,
            continent_name,
            country_name,
            latitude,
            longitude,
            type
        }));
    } catch (e) {

    }
}

function* watchCheckUserIpDispatch(): any {
    yield takeEvery('@IPS/CHECK_USER_IP', checkUserIpDispatch);
}

const ipSagas = [fork(watchCheckIpDispatch), fork(watchCheckUserIpDispatch)];

export default ipSagas;
